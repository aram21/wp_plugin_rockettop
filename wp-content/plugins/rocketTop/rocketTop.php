<?php
	/*
	Plugin Name: rocketTop
	Description: Plugin for scroll top.
	Version: 1.0
	Author: Aram Hovakimyan
	*/

?>

<!DOCTYPE html>
<html>
<head>
	<title>Rocket_Top</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>

	<style type="text/css">
		.scrollTop{
			display:none; 
			z-index:9999; 
			position:fixed; 
			bottom:10px; 
			left:48%; 
			color: #333;
		}

		.img {
			width: 150px;
			height: 130px;
		}
	</style>

<a href="#" class="scrollTop"><img class="img" src="rocket.gif"></a>

<script type="text/javascript">

$(function(){
	$(window).scroll(function(){
		// show-hide rocket
		var aTop = 300;
		if($(this).scrollTop() >= aTop) {
		    $(".scrollTop").css("display","block");
		}else {
		    $(".scrollTop").css("display","none");
		}

		$(".scrollTop").click(function(){
			$( 'html:not(:animated),body:not(:animated)' ).animate({ scrollTop: 0}, 500 );
		});

	});
});

</script>


</body>
</html>